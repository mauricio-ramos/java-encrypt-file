# java-encrypt-file

Simple file encryption


Generate key file using openssl


1- Key pair

openssl genrsa -out private_key.pem 2048

2- Public key

openssl rsa -in private_key.pem -pubout -outform DER -out public_key.der

3- Private key

openssl pkcs8 -topk8 -inform PEM -outform DER -in private_key.pem -out private_key.der -nocrypt