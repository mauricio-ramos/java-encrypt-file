import javax.crypto.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Properties;

public class Encrypt {

    private static String privateKeyFilePath;
    private static String publicKeyFilePath;
    private static String usersFilePath;
    private static String encryptFilePath;

    public static PrivateKey privateKey() {

        Path privateKeyPath = Paths.get(privateKeyFilePath);
        byte[] privateKeyBytes;
        PrivateKey privateKey = null;

        try {
            privateKeyBytes = Files.readAllBytes(privateKeyPath);
            privateKey = KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(privateKeyBytes));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return privateKey;
    }

    public static PublicKey publicKey() {

        Path publicKeyPath = Paths.get(publicKeyFilePath);
        byte[] publicKeyBytes;
        PublicKey publicKey = null;

        try {
            publicKeyBytes = Files.readAllBytes(publicKeyPath);
            publicKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(publicKeyBytes));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return publicKey;
    }

    public static void encryptFile(String file2encrypt, String filePath) {

        Cipher cipher;
        byte[] encryptedFile;

        try {
            cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, privateKey());
            encryptedFile = cipher.doFinal(file2encrypt.getBytes());

            BufferedWriter writer = new BufferedWriter(new FileWriter(filePath));
            writer.write(new String(Base64.getEncoder().encode(encryptedFile)));
            writer.close();

            System.out.println("Encryption completed. Output file: " + encryptFilePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void decryptFile(String encryptedFileB64) {

        Cipher cipher1;
        byte[] encryptedFile;
        byte[] decryptedFile;

        try {
            encryptedFile = Base64.getDecoder().decode(encryptedFileB64);
            cipher1 = Cipher.getInstance("RSA");
            cipher1.init(Cipher.DECRYPT_MODE, publicKey());
            decryptedFile = cipher1.doFinal(encryptedFile);

            System.out.println("********** Decrypted Data **********");
            System.out.println(new String(decryptedFile));
            System.out.println("************************************");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception{

        String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String propertiesPath = rootPath + "application.yml";

        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(propertiesPath));
        } catch (Exception e) {
            e.printStackTrace();
        }

        privateKeyFilePath = properties.getProperty("private-key-path");
        publicKeyFilePath = properties.getProperty("public-key-path");
        usersFilePath = properties.getProperty("users-file");
        encryptFilePath = properties.getProperty("encrypt-file");

        // Encrypt file
        File usersFile = new File(usersFilePath);
        FileInputStream usersFileIS = new FileInputStream(usersFile);
        byte[] data = new byte[(int) usersFile.length()];
        usersFileIS.read(data);
        usersFileIS.close();
        encryptFile(new String(data, "UTF-8"), encryptFilePath);

        // Decrypt file
        File encryptedFile = new File(encryptFilePath);
        FileInputStream encryptedFileIS = new FileInputStream(encryptedFile);
        byte[] dataEncrypt = new byte[(int) encryptedFile.length()];
        encryptedFileIS.read(dataEncrypt);
        encryptedFileIS.close();
        decryptFile(new String(dataEncrypt, "UTF-8"));
    }
}
